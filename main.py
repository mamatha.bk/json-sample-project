from scripts.core.convertion import Converter

details = dict()
name = input("Enter name:")
clg = input("Enter college name:")
dept = input("enter dept:")
num = int(input("enter number of courses:"))
details["name"] = name
details["College"] = clg
details["dept"] = dept
courses = []
for i in range(0, num):
    value = input("Enter course ")
    course_code = input("Enter course code")
    inner_contents = dict()
    inner_contents["course name"] = value
    inner_contents["course code"] = course_code
    courses.append(inner_contents)
details["courses"] = courses
obj = Converter.converter(details)
print(obj)
