import json


class Converter:
    @staticmethod
    def converter(details):
        result = json.dumps(details)
        return result
